import 'package:api_client_test/core/api/api_exception.dart';
import 'package:api_client_test/core/repositories/auth_repository.dart';

class AuthService{
  AuthRepository _authRepository;

  AuthService({AuthRepository authRepository}): _authRepository = authRepository;

  signIn({String user, String password, Function() onSuccess, Function(int code, String message) onError}) async {
    try {
      await _authRepository.login(user: user,password: password);
      onSuccess();
    } on ApiException catch (ex) {
      onError(ex.code, ex.apiError.message);
    } catch (Exception) {
      onError(1000, "Error unknown");
    }
  }
}