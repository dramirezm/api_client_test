import 'package:api_client_test/core/api/client.dart';
import 'package:api_client_test/core/services/auth_service.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

import 'repositories/auth_repository.dart';

  List<SingleChildWidget> apiProviders(String baseUrl){
    List<SingleChildWidget> providers =[
      // Provider.value(value: KimberlyApiClient.newInstance(endPoint: baseUrl)),
      Provider.value(value: Client.newInstance(baseUrl)),

      ProxyProvider<Client, AuthRepository>(
          update: (context, apiClient, repository) =>
              AuthRepository(apiClient: apiClient)),


      ProxyProvider<AuthRepository, AuthService>(
          update: (context, authRepository, authService) =>
              AuthService(authRepository: authRepository)),

    ];


    return providers;

  }