import 'package:api_client_test/core/api/client.dart';
import 'package:api_client_test/core/api/kimberly_api_client.dart';
import 'package:api_client_test/core/models/request/login_request.dart';
import 'package:fimber/fimber.dart';

class AuthRepository{
  Client _apiClient;

  AuthRepository({Client apiClient}):_apiClient = apiClient;

  Future<dynamic> login({String user, String password}) async {
    LoginRequest loginRequest = LoginRequest(user: user, password: password);

    var response = await _apiClient.signIn(loginRequest).catchError((
        Object obj) async {
      Fimber.d("LoginResponse Error");
      Client.throwException(obj);
    });

    return response;

  }

}