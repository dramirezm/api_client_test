import 'package:api_client_test/core/api/api_error.dart';
import 'package:api_client_test/core/api/api_exception.dart';
import 'package:api_client_test/core/models/request/login_request.dart';
import 'package:fimber/fimber_base.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'client.g.dart';

@RestApi(baseUrl: "")
abstract class Client {
  factory Client(Dio dio, {String baseUrl}) = _Client;

  static Client _client;
  static Dio _dio;

  @POST("CustomLogIn")
  Future<dynamic> signIn(@Body() LoginRequest loginRequest);


  static Client newInstance(String endPoint) {
    if (_dio == null) {
      _dio = Dio();
      _dio.options.headers["Content-Type"] = "application/json";
      _dio.options.headers["Accept"] = "application/json";
    } else {}


    _dio.interceptors.add(InterceptorsWrapper(onRequest: (options) async {
      Fimber.d("Headers ---> ${options.headers}");
      return options;
    }));

    // _dio.options.baseUrl = endPoint;
    _client = Client(_dio,baseUrl: endPoint);

    return _client;
  }

  static throwException(Object obj) {
    switch (obj.runtimeType) {
      case DioError:
        var hasInternet = true;
        final errorResponse = (obj as DioError).response;
        int code = 0;
        if (errorResponse != null) {
          try {
            code = errorResponse.statusCode;
          } catch (ex) {}
          Fimber.e("body : ${errorResponse.request.data}");
          Fimber.e("Error : $errorResponse");
        } else {
          hasInternet = false;
          Fimber.e("Error : NO INTERNET");
        }
        ApiError apiError;
        try {
          apiError = ApiError.fromJson(errorResponse.data);
        } catch (ex) {
          if (hasInternet) {
            code = 109;
            apiError = ApiError(message: "Unknown error");
          } else {
            code = 69;
            apiError = ApiError(message: "No internet connection");
          }
        }

        throw ApiException(code: code, apiError: apiError);
    }
  }

}
