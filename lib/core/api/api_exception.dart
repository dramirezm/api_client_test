import 'api_error.dart';

class ApiException implements Exception {
  final int code;
  final ApiError apiError;

  const ApiException({this.code, this.apiError});

  String toString() => "Code => $code\nMessage => ${apiError.message}";
}
