import 'package:api_client_test/core/api/client.dart';
import 'package:api_client_test/core/repositories/auth_repository.dart';
import 'package:api_client_test/core/services/auth_service.dart';
import 'package:provider/single_child_widget.dart';

class KimberlyApiClient {

  static KimberlyApiClient _apiClient;
  Client _client;
  AuthRepository _authRepository;
  AuthService _authService;

  static KimberlyApiClient newInstance({String endPoint}) {
    if (_apiClient == null) {

      _apiClient = KimberlyApiClient();

      _apiClient.init(endPoint: endPoint);
    }

    return _apiClient;
  }

  init({String endPoint}) {
      _client = Client.newInstance(endPoint);
      initAuth();
  }

  initAuth(){
    _authRepository = AuthRepository(apiClient: client);
    _authService = AuthService(authRepository: _authRepository);
  }


  //Services getters

  AuthService get authService{
    if(_authService  == null){
      return initAuth();
    }

    return _authService;
  }


  //Client getter
  Client get client {
    if (_client == null) {
      init();
    }
    return _client;
  }

}
