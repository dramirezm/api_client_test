import 'package:api_client_test/core/models/users/child.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User{
  @JsonKey(name: '_id')
  final int id;
  @JsonKey(name: 'email')
  final int email;
  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'imageUrl')
  final String photo;
  @JsonKey(name: 'address')
  final String address;
  @JsonKey(name: 'username')
  final String userName;
  @JsonKey(name: 'totalPoints')
  final String totalPoints;
  @JsonKey(name: 'child')
  final Child child;


  User({this.id,this.email,this.name,this.photo,this.child,this.address,this.userName,this.totalPoints});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}