import 'package:json_annotation/json_annotation.dart';

part 'child.g.dart';

@JsonSerializable()
class Child{
  @JsonKey(name: '_id')
  final int id;
  @JsonKey(name: 'age')
  final int age;
  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'imageUrl')
  final String photo;
  @JsonKey(name: 'gender')
  final String gender;


  Child({this.id,this.age,this.name,this.photo,this.gender});

  factory Child.fromJson(Map<String, dynamic> json) => _$ChildFromJson(json);

  Map<String, dynamic> toJson() => _$ChildToJson(this);
}