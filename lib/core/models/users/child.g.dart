// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'child.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Child _$ChildFromJson(Map<String, dynamic> json) {
  return Child(
    id: json['_id'] as int,
    age: json['age'] as int,
    name: json['name'] as String,
    photo: json['imageUrl'] as String,
    gender: json['gender'] as String,
  );
}

Map<String, dynamic> _$ChildToJson(Child instance) => <String, dynamic>{
      '_id': instance.id,
      'age': instance.age,
      'name': instance.name,
      'imageUrl': instance.photo,
      'gender': instance.gender,
    };
