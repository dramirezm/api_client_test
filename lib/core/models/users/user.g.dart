// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['_id'] as int,
    email: json['email'] as int,
    name: json['name'] as String,
    photo: json['imageUrl'] as String,
    child: json['child'] == null
        ? null
        : Child.fromJson(json['child'] as Map<String, dynamic>),
    address: json['address'] as String,
    userName: json['username'] as String,
    totalPoints: json['totalPoints'] as String,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      '_id': instance.id,
      'email': instance.email,
      'name': instance.name,
      'imageUrl': instance.photo,
      'address': instance.address,
      'username': instance.userName,
      'totalPoints': instance.totalPoints,
      'child': instance.child,
    };
