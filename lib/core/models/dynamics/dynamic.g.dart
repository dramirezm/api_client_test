// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dynamic.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Dynamic _$DynamicFromJson(Map<String, dynamic> json) {
  return Dynamic(
    id: json['_id'] as int,
    url: json['urlMedia'] as String,
    type: json['type'] as String,
  );
}

Map<String, dynamic> _$DynamicToJson(Dynamic instance) => <String, dynamic>{
      '_id': instance.id,
      'urlMedia': instance.url,
      'type': instance.type,
    };
