import 'package:json_annotation/json_annotation.dart';

part 'dynamic.g.dart';

@JsonSerializable()
class Dynamic{
  @JsonKey(name: '_id')
  final int id;
  @JsonKey(name: 'urlMedia')
  final String url;
  @JsonKey(name: 'type')
  final String type;

  Dynamic({this.id,this.url,this.type});

  factory Dynamic.fromJson(Map<String, dynamic> json) => _$DynamicFromJson(json);

  Map<String, dynamic> toJson() => _$DynamicToJson(this);
}