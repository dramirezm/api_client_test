import 'package:json_annotation/json_annotation.dart';

part 'new.g.dart';

@JsonSerializable()
class New{
  @JsonKey(name: '_id')
  final int id;
  @JsonKey(name: 'title')
  final String title;
  @JsonKey(name: 'content')
  final String content;

  New({this.id,this.title,this.content});

  factory New.fromJson(Map<String, dynamic> json) => _$NewFromJson(json);

  Map<String, dynamic> toJson() => _$NewToJson(this);
}